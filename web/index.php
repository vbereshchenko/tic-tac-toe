<?php

use Ai\EnemyBot;
use Composer\Autoload\ClassLoader;
use Field\Field;
use Game\Combination\Checker;
use Game\Draw;
use Game\Type;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @var $autoload ClassLoader
 */
$autoload = require '../vendor/autoload.php';
$autoload->addPsr4('', '../src');

$app = new Silex\Application();
$app->register(new \Silex\Provider\SessionServiceProvider());

$app['serialize'] = function() {
    return function($data) {
        return new JsonResponse($data);
    };
};

$app->get('/start/type/{type}', function($type) use ($app) {
    $field = new Field(3);
    $player = Type::CROSS == $type ? Type::CROSS : Type::ZERO;

    $app['session']->set('field', $field);
    $app['session']->set('player', $player);

    if(Type::ZERO == $player) {
        $game = new TicTacToe($field, new Checker($field->getField(), $field->getSize()));
        $game->setAi(new EnemyBot($field));
        $game->oppositeDraw(null, Type::CROSS);
    }

    return $app['serialize']([
        'status' => 'starting',
        'field' => $field->getField()
    ]);
});

$app->get('/draw/{x}/{y}', function($x, $y) use ($app) {
    if(!$app['session']->has('field')) {
        return $app['serialize']([
            'status' => 'fail',
            'error' => 'Field is not initialized. Request /start/type/{type}, please'
        ]);
    }

    /**
     * @var $field Field
     */
    $field = $app['session']->get('field');
    $player = $app['session']->get('player');
    $game = new TicTacToe($field, new Checker($field->getField(), $field->getSize()));

    $game->setAi(new EnemyBot($field));

    $draw = new Draw($x, $y, $player);

    try {
        $game->draw($draw);
        $game->oppositeDraw($draw);
    } catch(Exception $e) {
        return $app['serialize']([
            'status' => 'failed',
            'error' => $e->getMessage(),
            'field' => $field->getField()
        ]);
    }

    if($win = $game->checkWin($field)) {

        $zeroWon = array_sum($win) == Type::ZERO;

        return $app['serialize']([
            'status' => $zeroWon == $player ? 'won' : 'lost',
            'combination' => $win
        ]);
    }

    $app['session']->set('field', $field);

    return $app['serialize']([
        'status' => 'playing',
        'field' => $field->getField()
    ]);
});

$app->run();