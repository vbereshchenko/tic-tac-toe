#!/bin/sh

puppet module install ajcrowe-supervisord
puppet module install saz-ssh
puppet module install jfryman-nginx
puppet module install puppetlabs/git
puppet module install example42-yum
puppet module install Slashbunny-phpfpm