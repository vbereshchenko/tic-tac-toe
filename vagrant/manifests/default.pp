hiera_include('classes')

$php_packages = hiera_array('php::packages')

Class[yum] -> Package[php56w-fpm] -> Package[$php_packages] ~> Service[php-fpm]

package { $php_packages:
  ensure => installed,
}

augeas { 'php_config':
  context => '/files/etc/php.ini/PHP',
  notify  => Service[php-fpm],
  changes => [
    'set date.timezone Europe/Kiev',
    'set memory_limit 128M',
    'set max_execution_time 3600',
    'set short_open_tag off'
  ]
}

class { 'phpfpm':
  package_name => php56w-fpm,
  process_max  => 20,
  log_level    => 'warning',
  error_log    => '/var/log/phpfpm.log',
}

class { 'supervisord':
  install_pip => true,
  unix_socket => true,
  unix_socket_mode => '0777',
  unix_auth => true,
  unix_username => 'deploy',
  unix_password => '123'
}

supervisord::supervisorctl { 'update_worker':
  command => 'update',
  process => 'worker'
}

phpfpm::pool { 'www':
  user                   => hiera('php_fpm_user'),
  group                  => hiera('php_fpm_group'),
  listen                 => '127.0.0.1:9000',
  listen_allowed_clients => '127.0.0.1',
  pm                     => 'dynamic',
  pm_max_children        => 10,
  pm_start_servers       => 4,
  pm_min_spare_servers   => 2,
  pm_max_spare_servers   => 6,
  pm_max_requests        => 500,
  pm_status_path         => '/status',
  ping_path              => '/ping',
  ping_response          => 'pong',
  env                    => {
    'ODBCINI' => '"/etc/odbc.ini"',
  },
  php_admin_flag         => {
    'expose_php' => 'Off',
  },
  php_admin_value        => {
    'max_execution_time' => '300',
    'log_errors' => 'On',
    'log_errors' => '/var/log/php-fpm/error.log',
  },
}

exec { 'install_composer':
  command  => 'curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer',
  require  => Package[php56w-cli],
  cwd      => '/tmp',
  path     => ["/usr/bin", "/bin"]
}

if versioncmp($::puppetversion,'3.6.1') >= 0 {
  $allow_virtual_packages = hiera('allow_virtual_packages',false)
  Package {
    allow_virtual => $allow_virtual_packages,
  }
}

node 'tictactoe.dev' {

  ssh::server::host_key { 'ssh_host_rsa_key':
    private_key_source => '/vagrant/ssh_host_rsa_key',
    public_key_source  => '/vagrant/ssh_host_rsa_key.pub',
  }

  file { "/root/.ssh":
    ensure => "directory",
  }

  file { "/root/.ssh/id_rsa":
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => 600,
    source  => "/etc/ssh/ssh_host_rsa_key",
    require => Class[ssh::server]
  }

  file { "/root/.ssh/id_rsa.pub":
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => 600,
    source  => "/etc/ssh/ssh_host_rsa_key.pub",
    require => Class[ssh::server]
  }

  augeas { 'php_config_xdebug':
    context => '/files/etc/php.ini/PHP',
    notify  => Service[php-fpm],
    changes => [
      'set xdebug.max_nesting_level 500'
    ]
  }
}