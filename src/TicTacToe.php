<?php

use Ai\BotInterface;
use Field\Field;
use Game\Combination\CheckerInterface;
use Game\Draw;
use Game\TicTacToeInterface;

class TicTacToe implements TicTacToeInterface
{
    /**
     * @var Field
     */
    private $field;

    /**
     * @var bool
     */
    private $turn = false;

    /**
     * @var BotInterface
     */
    private $handler;

    /**
     * @var CheckerInterface
     */
    private $checker;

    /**
     * @param Field $field
     * @param CheckerInterface $checker
     */
    public function __construct(Field $field, CheckerInterface $checker)
    {
        $this->field = $field;
        $this->checker = $checker;
    }

    /**
     * @param Draw $draw
     * @throws \Game\Exception\CellAlreadyMarked
     */
    public function draw(Draw $draw)
    {
        $this->field->draw($draw);
    }

    /**
     * @param null $draw
     * @param null $type
     * @throws \Game\Exception\CellAlreadyMarked
     */
    public function oppositeDraw($draw = null, $type = null)
    {
        $oppositeDraw = null;
        if(null !== $draw) {
            $oppositeDraw = $this->handler->draw($draw);
        } else {
            $oppositeDraw = new Draw(1, 1, $type);
        }

        $this->field->draw($oppositeDraw);
    }

    /**
     * @param BotInterface $handler
     */
    public function setAi(BotInterface $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param Field $field
     * @return []
     */
    public function checkWin(Field $field)
    {
        if($combination = $this->checker->horizontal()) {
            return $combination;
        }

        if($combination = $this->checker->vertical()) {
            return $combination;
        }

        if($combination = $this->checker->hypotenuse()) {
            return $combination;
        }

        return null;

    }

}