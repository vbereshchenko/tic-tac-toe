<?php

namespace Game;

use Ai\BotInterface;
use Field\Field;
use Game\Combination\CheckerInterface;

interface TicTacToeInterface
{

    /**
     * Initialization with checker
     * to identify state of current field
     *
     * @param Field $field
     * @param CheckerInterface $checker
     */
    function __construct(Field $field, CheckerInterface $checker);

    /**
     * @param Draw $draw
     * @return void
     */
    function draw(Draw $draw);

    /**
     * @param BotInterface $ai
     */
    function setAi(BotInterface $ai);

    /**
     * @param Field $field
     * @return []
     */
    function checkWin(Field $field);
}