<?php

namespace Game;

class Draw
{
    /**
     * @var int
     */
    private $positionX;

    /**
     * @var int
     */
    private $positionY;

    /**
     * @var Type
     */
    private $type;

    /**
     * @param int $positionX
     * @param int $positionY
     * @param Type $type
     */
    public function __construct($positionX, $positionY, $type)
    {
        $this->positionX = $positionX;
        $this->positionY = $positionY;
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getPositionX()
    {
        return $this->positionX;
    }

    /**
     * @return int
     */
    public function getPositionY()
    {
        return $this->positionY;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

}