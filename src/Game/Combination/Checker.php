<?php

namespace Game\Combination;


class Checker implements CheckerInterface
{

    /**
     * @var array
     */
    private $coordinates = [];

    /**
     * @var int
     */
    private $size;

    public function __construct(array $coordinates, $size)
    {
        $this->coordinates = $coordinates;
        $this->size = $size;
    }

    public function horizontal()
    {
        for($x=0; $x<$this->size; $x++) {
            if($this->containsNull($this->coordinates[$x]->toArray())) {
                continue;
            }
            $sum = array_sum($this->coordinates[$x]->toArray());
            if($sum == 0 || $sum == $this->size) {
                return [
                    [$x, 0],
                    [$x, 1],
                    [$x, 2]
                ];
            }
        }
    }

    public function vertical()
    {
        for($x=0; $x<$this->size; $x++) {
            $combination = [];
            $sum = 0;
            for($y=0;$y<$this->size; $y++) {
                $value = $this->coordinates[$x][$y];
                if(null === $value) {
                    $combination = null;
                    break;
                }
                $combination[] = [$x, $y];
                $sum += $value;
            }
            if($combination !== null && $sum == 0 || $sum == $this->size) {
                return $combination;
            }
        }

        return null;
    }

    public function hypotenuse()
    {
        $combination = [];
        for($i=0; $i<$this->size; $i++) {
            $combination[] = $this->coordinates[$i][$i];
        }

        $sum = array_sum($combination);
        if($sum == 0 || $sum == $this->size) {
            return [
                [0,0],
                [0, 1],
                [1, 2]
            ];
        }

        $combination = [];

        for($i=0; $i<$this->size; $i++) {
            $combination[] = $this->coordinates[2-$i][2-$i];
        }

        $sum = array_sum($combination);
        if($sum == 0 || $sum == $this->size) {
            return [
                [2,2],
                [2, 1],
                [1, 0]
            ];
        }
    }

    private function containsNull(array $arr) {
        foreach($arr as $value) {
            if(is_null($value)) {
                return true;
            }
        }

        return false;
    }
}