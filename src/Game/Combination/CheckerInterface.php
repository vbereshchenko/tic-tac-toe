<?php

namespace Game\Combination;


interface CheckerInterface
{

    function __construct(array $coordinates, $size);

    /**
     * Checks if there're any combinations on horizontal lines
     *
     * @return []
     */
    function horizontal();

    /**
     * Checks if there're any combinations on vertical lines
     *
     * @return []
     */
    function vertical();

    /**
     * Checks if there're any combinations on hypotenuses
     *
     * @return []
     */
    function hypotenuse();
}