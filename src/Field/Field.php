<?php

namespace Field;

use Game\Draw;
use Game\Exception\CellAlreadyMarked;
use InvalidArgumentException;
use SplFixedArray;

class Field
{
    /**
     * @var int[][]
     */
    private $field;

    /**
     * @var int
     */
    private $size;

    /**
     * @param $size int
     */
    public function __construct($size)
    {
        if($size <= 0)
            throw new InvalidArgumentException("Size of the field should be higher than 0");

        $this->size = (int) $size;
        $this->field = new SplFixedArray($this->size);
        $this->fillColumns();
    }

    /**
     * @param Draw $draw
     * @throws CellAlreadyMarked
     */
    public function draw(Draw $draw)
    {
        $x = $draw->getPositionX();
        $y = $draw->getPositionY();

        $this->validateDraw($draw);

        if($this->isMarked($draw)) {
            throw new CellAlreadyMarked(sprintf(
                "Cell x:%s y:%s already marked",
                $x, $y
            ));
        }

        $this->field[$x][$y] = $draw->getType();
    }

    /**
     * @return int[][]
     */
    public function getField()
    {
        return $this->field->toArray();
    }

    public function validateDraw(Draw $draw)
    {
        $x = $draw->getPositionX();
        $y = $draw->getPositionY();
        if($x < 0 || $x > $this->size || $y < 0 || $y > $this->size) {
            throw new InvalidArgumentException(sprintf(
                "Coordinates should be in range [0;%s]",
                $this->size
            ));
        }
    }

    /**
     * Checks if cell was already used
     *
     * @param Draw $draw
     * @return bool
     */
    public function isMarked(Draw $draw)
    {
        return $this->isPositionMarked($draw->getPositionX(), $draw->getPositionY());
    }

    /**
     * @param int $x
     * @param int $y
     * @return bool
     */
    public function isPositionMarked($x, $y)
    {
        return !is_null($this->field[$x][$y]);
    }

    /**
     * @return array
     */
    public function getFreePositions()
    {
        $positions = [];
        for($x = 0; $x<$this->size; $x++) {
            for($y = 0; $y<$this->size; $y++) {
                if($this->isPositionMarked($x, $y)) {
                    $positions[] = [$x, $y];
                }
            }
        }

        return $positions;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    private function fillColumns()
    {
        for($i=0; $i < $this->size; $i++) {
            $this->field[$i] = new \SplFixedArray($this->field->getSize());
        }
    }

}