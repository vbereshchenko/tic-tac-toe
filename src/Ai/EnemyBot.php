<?php

namespace Ai;


use Field\Field;
use Game\Draw;
use Game\Type;

class EnemyBot implements BotInterface
{

    /**
     * @var Field
     */
    private $field;

    /**
     * @param Field $field
     */
    public function __construct(Field $field)
    {
        $this->field = $field;
    }

    public function draw(Draw $playerDraw)
    {
        $type = abs($playerDraw->getType() - Type::CROSS); // inverting role

        if($playerDraw->getPositionX() == 1 && $playerDraw->getPositionY() == 1) {
            $draw = new Draw(2, 2, $type);
            if($this->field->isMarked($draw)) {
                return $this->generateRandom($type);
            }

            return $draw;
        }

        $opDraw = new Draw(2 - $playerDraw->getPositionX(), 2 - $playerDraw->getPositionY(), $type);
        // trying to make player loose
        if($this->field->isMarked($opDraw)) {
            return $this->generateRandom($type);
        }

        return $opDraw;
    }

    private function generateRandom($type)
    {
        $positions = $this->field->getFreePositions();
        $position = $positions[array_rand($positions)];

        return new Draw($position[0], $position[1], $type);
    }

}