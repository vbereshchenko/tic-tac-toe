<?php

namespace Ai;


use Game\Draw;

interface BotInterface
{
    function draw(Draw $playerDraw);
}